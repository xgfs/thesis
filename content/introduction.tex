\chapter{Introduction}

\section{Motivation}

Graph structure arises in many real-world applications with examples such as social networks, co-authorship networks, and World Wide Web. The scale of the data in these networks varies from hundreds to billions of nodes and edges, posing unique challenges of large-scale learning. One approach for dealing with the sparsity of the graph is to learn the \textbf{representation} of the graph: each vertex in the network is represented by a low-dimensional vector that can be useful in various tasks, such as visualization~\cite{van2008visualizing}, node classification~\cite{bhagat2011node, girvan2002community, tang2009relational}, and link prediction~\cite{liben2007link}.

Embedding in a low-dimensional space is not new nor exclusive to graphs: Hinton proposed that concepts can be embedded in low-dimensional space in 1986~\cite{hinton1986learning}, embeddings are extremely common in natural language processing (NLP) tasks~\cite{dumais1988using,bengio2006neural,mikolov2010recurrent,bottou2011machine,collobert2011natural}. One of the most notable examples is \wv~\cite{mikolov2013efficient}, used in natural language processing with great success, surpassing the traditional baselines in various NLP tasks. This method is widely studied: Levy and Goldberg showed~\cite{levy2014neural} that \wv~is essentially factorizing pointwise mutual information matrix for words and their context. 

%todo: image and text representation, deep learning success

Real-world networks are usually too large to apply traditional graph embedding methods, such as Multidimensional Scaling (MDS)~\cite{cox2000multidimensional}, IsoMap~\cite{tenenbaum2000global}, Locally Linear Embedding (LLE)~\cite{roweis2000nonlinear}, and Laplacian Eigenmaps~\cite{belkin2001laplacian}. One notable method is presented by Shaw and Jebara~\cite{shaw2009structure}, allowing embedding to preserve graph structure.

Methods mentioned have at least quadratic complexity, which effectively makes them infeasible for real-world graphs. Recently developed methods~\cite{perozzi2014deepwalk,tang2015line,cao2015grarep} address the problem of embedding large-scale graphs. Besides that problem, many real-world graphs have additional information stored on the nodes and edges. For example, in social networks user may input his sex, university, city, and more information that has to be addressed by the graph embedding model. There are generative graph models that address this peculiarity~\cite{xu2012model, pfeiffer2014attributed}, yet graph embeddings are still lacking on this aspect.

\section{Related work}

\subsection{Classical Results and Simple Techniques}
%todo: add traditional graph embeddings, make a section out of it

\subsection{Neural word embeddings}
%todo: Add Niu paper
%todo: Just short description there, no pictures

Recent developments in the field of representation learning on graphs arguably started with the publication of the original \wv~paper~\cite{mikolov2013efficient}, introducing two distinct models: \textbf{continuous bag-of-words} (CBOW), shown on figure~\ref{fig:cbow}, and \textbf{skip-gram} model depicted schematically on figure~\ref{fig:skip-gram}.

 Under the skip-gram model, we are given a corpus $D$ of words $w$ and their respective contexts $c$. Then, the model task is to set the parameters $\theta$ in order to maximize the word-context probability over the corpus:

\begin{equation}
	\label{eq:skipgram-words}
	\arg\max_\theta \prod_{(w,c)\in D}{p(c|w;\theta)}
\end{equation}

\wv~approach to parametrizing this model follows the neural network language model literature~\cite{bengio2006neural,mikolov2010recurrent}, and uses Softmax function:

\begin{equation}
	\label{eq:softmax-words}
	p(c|w;\theta) = \frac{e^{v_c \cdot v_w}}{\sum_{c' \in C}{e^{v_{c'} \cdot v_w}}}
\end{equation}

where $v_c$ and $v_w$ is the context and word vectors in $\mathcal{R}^d$ respectively, and $C$ is the set of all contexts. Next, logarithm of the objective function is taken, and we switch from product to sum:

\begin{equation}
	\label{eq:logsoftmax-words}
	\arg\max_\theta \sum_{(w,c)\in D}{\log{p(c|w;\theta)}} = \sum_{(w,c)\in D}{\left( \log{e^{v_c \cdot v_w}} - \log{\sum_{c' \in C}{e^{v_{c'} \cdot v_w}}}\right)}
\end{equation}

In this form, the objective function can be computed and efficiently optimized on medium-sized corpora. For larger corpus, and, thus, dictionary sizes, $\log{\sum_{c' \in C}{e^{v_{c'} \cdot v_w}}}$ is very expensive to compute, as we may have hundreds of thousands of words as contexts. To overcome this problem, \textbf{hierarchical softmax} is introduced in~\cite{morin2005hierarchical,mnih2009scalable}. Explanation of hierarchical softmax is out of the scope of this report, for more information it is recommended to follow~\cite{rong2014word2vec}.

\begin{figure}[htbp]
\centering
\includegraphics[height=8cm]{images/CBOW}
\caption{CBOW model.}
\label{fig:cbow}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[height=8cm]{images/SGNS.pdf}
\caption{The skip-gram model.}
\label{fig:skip-gram}
\end{figure}

Another approach to tackling the softmax computational complexity is using a technique called \textbf{negative sampling}~\cite{mikolov2013distributed}. This idea is far more straightforward than hierarchical softmax: in order to deal with the difficulty of having too many output vectors that need to be updated per iteration, we only update a sample of them. 

There are two notable modifications of classic \wv~that are useful for future work. The first one is presented in the paper~\cite{blacktwo}. For the traditional skip-gram model, word order does not matter, it is fixed in a na\"{\i}ve scheme by the authors of~\cite{blacktwo}.

Second modification that is useful for our real-world graph modeling tasks is presented in~\cite{niu2015unified} that is important for the scope of this paper: na\"{\i}ve attributed variants for CBOW~(fig. XXX) and skip-gram~(fig.~\ref{fig:niu-skip-gram}) \wv~architectures, allowing to learn the embeddings by solving two tasks (representation and attribute prediction) jointly. In the skip-gram approach, attributes $A_1 \cdots A_k$ are treated as additional input that enhance prediction. In the CBOW model, attributes of the central word are predicted by the context words.

\begin{figure}[htbp]
\centering
\includegraphics[height=8cm]{images/SGNS-Niu}
\caption{The attributed skip-gram model.}
\label{fig:niu-skip-gram}
\end{figure}


\subsection{Neural graph embeddings}

In 2014, \textbf{DeepWalk} paper~\cite{perozzi2014deepwalk} was published, deeply linking word and vertex embeddings on several levels. The connection that now seems natural and trivial is to map each vertex $v_i$ in the graph $G$ to distinct word $w_{v_i}$. Then, we draw random walk ``sentences'' from the graph, and feed them to the skip-gram model. Note that truncated random walks are proved to being able to capture the local graph structure of the graph, thus, the prepositions of DeepWalk matches language modeling ones: vertices with similar neighborhoods (contexts) will be represented in a similar way. Recent paper~\cite{yang2015comprehend} showed that DeepWalk implicitly decomposes the matrix of transition log-probabilities.

DeepWalk proved to be an efficient algorithm, allowing classification~\cite{perozzi2014deepwalk} and exact age prediction~\cite{perozzi2015exact} with as little as $5\%$ training data. This is extremely important because in real-world networks information is incomplete and noisy.

Theoretically important results were published by Hashimoto et al. in~\cite{hashimoto2014metric,hashimoto2015word}. They unify \wv--based methods in terms of metric recovery, proposing a stable metric to evaluate these models. The main idea is that if the graph comes from a metric space, good embedding will recover that metric. However, their results are highly theoretical, and it is unclear whether the theoretical recommendations that are given in the paper will be backed up in the real-world graphs.

Another framework for graph embedding, LINE, is presented in~\cite{tang2015line}. Authors limit their objective function to second-order vertex proximity and claim their objective function is ``cleaner'' than DeepWalk one and their objective function lead to structure preservation. They also claim that DeepWalk only applied to unweighted undirected graphs, although it can be extended to both easily. There is not enough proof for any of these claims in the paper, and the overall approach seems even more black-box than DeepWalk.

Similar to LINE, GraRep is introduced in~\cite{cao2015grarep}. It extend the notion of neighbors to $k$ steps, concatenating embeddings obtained for each step-$k$ transition probability matrices. This approach relied on explicit matrix factorization, which is a thing that \wv~and DeepWalk tried avoid as being impractical for large datasets. Indeed, largest graph processed by GraRep authors was at ten thousand vertices, which is much smaller than real-world graphs.

Heterogeneous Network Embedding presented in~\cite{chang2015heterogeneous} presented an interesting approach to learning embedding of heterogeneous networks, specifically networks with images and texts that are interconnected. Their deep network is constructed from modules that are sharing weights, resembling multiple siamese networks with shared weights~\cite{bromley1993signature}. The model allows getting good cross-model retrieval results, significantly improving over methods that use just the content of the document. 

\section{Main Contributions}
\subsection{Overview}
\subsection{Graph Embedding}
\subsection{Attributed Graph Embedding}

\section{Outline}
We describe basic concepts in chapter 2, blah